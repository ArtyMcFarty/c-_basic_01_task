﻿using System;

namespace ConsolPrj1
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Type your name ");
            string firstName = Console.ReadLine();
            Console.WriteLine("Type your last name ");
            string lastName = Console.ReadLine();

            Console.WriteLine($"Welcome to console {firstName} {lastName}!");
            Console.WriteLine("It's project ConsolPrj1");

        }
    }
}
