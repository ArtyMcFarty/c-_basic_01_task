﻿using System;
using LibPrj3;
namespace ConsolPrj2
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Type some text! ");
            string someText = Console.ReadLine();
            Console.WriteLine($"\nYou wrote this text: {someText}");

            //Getting name of project
            Type projectName = typeof(MainClass);
            Console.WriteLine("The name of project is: {0}.", projectName.Namespace);

            int a = 10, b = 6;
            Console.WriteLine(Arithmetic.Add(a, b));
            Console.WriteLine(Arithmetic.Subtract(a, b));
            Console.WriteLine(Arithmetic.Multiply(a, b));
            Console.WriteLine(Arithmetic.Divide(a, b));
        }
    }
}
